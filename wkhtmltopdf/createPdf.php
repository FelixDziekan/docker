<?php

$html = "<html><body>Hello world</body></html>";

// set request body
$body = json_encode([
    "contents" => base64_encode($html),
    "options"  => ["page-size" => "Letter"] // remove margins using: ["B" => "0", "L" => "0", "R" => "0", "T" => "0"]
]);

// set header
$headers = [
    "Content-Type: application/json",
    "Content-Length: ".strlen($body),
];
echo "test";

// curl options
$options = [
    CURLOPT_URL            => "http://localhost/",
    CURLOPT_PORT           => 8080,
    CURLOPT_POST           => 1,
    CURLOPT_POSTFIELDS     => $body,
    CURLOPT_HTTPHEADER     => $headers,
    CURLOPT_RETURNTRANSFER => true
];

echo "test";
// curl call
$ch = curl_init();
curl_setopt_array($ch, $options);
$result = curl_exec($ch);
curl_close($ch);

echo $result;
// print result
print_r(json_decode($result, true));
