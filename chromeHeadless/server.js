const express = require("express");
const puppeteer = require("puppeteer");
const fs = require("fs").promises;
const path = require("path");

const app = express();

// Middleware to parse JSON and URL-encoded data
app.use(express.json({ limit: "10mb" })); // Adjust the limit according to your needs
app.use(express.urlencoded({ extended: true, limit: "10mb" })); // Also increase the limit for URL-encoded bodies if necessary

function generateLongerFilename(id, classType) {
  if (id === undefined) {
    return res.status(400).send({
      message: "No EntityId.",
    });
  }

  if (classType === undefined) {
    return res.status(400).send({
      message: "No classType.",
    });
  }

  // Generate a random string of 25 characters
  const randomString = [...Array(25)]
    .map(() => Math.random().toString(36)[2])
    .join("");
  return `temp-${id}-${classType}-${randomString}.html`;
}

app.post("/", async (req, res) => {
  try {
    console.log("Converting HTML to PDF");
    const { contents, options, entityId, classType } = req.body;

    if (!contents) {
      return res.status(400).send({
        message: "Base64 encoded HTML contents are required in the body.",
      });
    }

    // Decode Base64 HTML contents
    const htmlContent = Buffer.from(contents, "base64").toString("utf-8");

    // Write to a temporary HTML file with a random name
    const fileName = generateLongerFilename(entityId, classType);
    const filePath = path.join(__dirname, fileName);
    await fs.writeFile(filePath, htmlContent);
    console.log(filePath);
    const browser = await puppeteer.launch({
      headless: true,
      executablePath: "/usr/bin/google-chrome",
      args: ["--no-sandbox", "--disable-gpu"],
    });

    const page = await browser.newPage();
    await page.goto(`file://${filePath}`, { waitUntil: "networkidle0" });

    await page.emulateMediaType("print");

    // Prepare PDF options
    const pdfOptions = {
      printBackground: true,
      margin: {
        // Set all margins to 0 to avoid added borders
        top: "0cm",
        right: "0cm",
        bottom: "0cm",
        left: "0cm",
      },
    };

    if (options.width && options.height) {
      pdfOptions.width = options.width;
      pdfOptions.height = options.height;
    } else {
      pdfOptions.format = options["page-size"] || "A4"; // Default to A4 if page-size is not specified
    }

    const pdfBuffer = await page.pdf(pdfOptions);
    await browser.close();

    // Delete the temporary file
    await fs.unlink(filePath);

    // Send the PDF in the response
    res.set("Content-Type", "application/pdf");
    res.send(pdfBuffer);
    console.log("PDF conversion completed");
  } catch (error) {
    console.error("Error processing request:", error);
    res.status(500).send({ message: "Internal server error" });
  }
});

app.listen(9000, () => {
  console.log("Listening on port 9000");
});
