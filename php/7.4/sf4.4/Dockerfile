FROM php:7.4-fpm-alpine

RUN apk  --update add \
    php7-bcmath php7-bz2 php7-cli php7-common php7-curl \
    php7-dev php7-fpm php7-gd php7-gmp php7-imap php7-intl \
    php7-json php7-ldap php7-mbstring php7-mcrypt php7-mysqli \
    php7-odbc php7-opcache php7-pgsql php7-phpdbg php7-pspell \
    php7-pdo_mysql php7-pdo php7-pdo_dblib \
    readline php7-soap php7-sqlite3 \
    php7-tidy php7-xml php7-xmlrpc php7-xsl php7-zip\
    openssh-client sed git\
    jpeg-dev libpng-dev freetype-dev\
    imagemagick imagemagick-libs imagemagick-dev\
    icu-libs zlib libzip libzip-dev


ENV APCU_VERSION 5.1.20

RUN set -xe \
        && apk add --no-cache --virtual .build-deps \
                $PHPIZE_DEPS \
                icu-dev \
                zlib-dev \
        && docker-php-ext-install \
                intl \
                pdo_mysql \
                zip \
        && pecl install \
                apcu-${APCU_VERSION} \
                apcu_bc \
                imagick \
                redis \
        && docker-php-ext-enable --ini-name 20-apcu.ini apcu \
        && docker-php-ext-enable --ini-name 21-apc.ini apc \
        && docker-php-ext-enable --ini-name 05-opcache.ini opcache \
        && docker-php-ext-enable --ini-name 20-imagick.ini imagick


# Install Composer
RUN curl -sS https://getcomposer.org/installer -o composer-setup.php
RUN php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer

RUN docker-php-ext-install opcache pdo_mysql mysqli
RUN docker-php-ext-enable opcache

COPY php.ini /etc/php7/
COPY www.conf /etc/php7/php-fpm.d/www.conf
COPY docker-php-memlimit.ini /usr/local/etc/php/conf.d/

#COPY php7.4-fpm.conf /etc/apache2/conf-available/php7.4-fpm.conf
#RUN a2enconf php7.4-fpm

